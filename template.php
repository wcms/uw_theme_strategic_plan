<?php

/**
 * @file
 */

/**
 * Return a themed breadcrumb trail.
 *
 * @param array $variables
 *   An array containing the breadcrumb links.
 *
 * @return string
 *   A string containing the breadcrumb output.
 */
function uw_theme_strategic_plan_breadcrumb(array $variables) {
  $breadcrumb = $variables['breadcrumb'];

  // Start all breadcrumbs by linking to the Strategic Plan site.
  array_unshift($breadcrumb, l(t('Strategic plan'), '/strategic-plan', array('external' => TRUE)));

  if (drupal_is_front_page()) {
    // Show the site name at the end of the breadcrumb.
    $breadcrumb[1] = variable_get('site_name', 'Site home');
  }
  else {
    // Replace the 'home' link with the site name.
    $breadcrumb[1] = l(variable_get('site_name', 'Site home'), '<front>');

    // Add the current page title to the end of the breadcrumb.
    $breadcrumb[] = drupal_get_title();
  }

  // Return the breadcrumb with separators.
  if (!empty($breadcrumb)) {
    $separator = ' › ';

    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $heading = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    return $heading . '<div class="breadcrumb">' . implode($separator, $breadcrumb) . '</div>';
  }
}

/**
 *
 */
function _uw_theme_strategic_plan_key_tab($content) {
  if (!isset($_GET['tab']) || $_GET['tab'] == 'key') {
    ?>
    <h2 class="element-invisible">Progress dashboards</h2>
    <?php
    print '<div class="active-background">';
    $showtableau = isset($_GET['tableau']) && !empty($content['field_key_indicator'][$_GET['tableau']]);

    if (!empty($content['field_key_indicator']['#items'])) {
      print '<div id="key-indicators"';
      if (!$showtableau) {
        print ' class="active"';
      }
      print '>';
      foreach ($content['field_key_indicator']['#items'] as $id => $item) {
        print '<div>';
        print '<a href="?tab=key&tableau=' . $id . '#tabs-nav">';
        print render($content['field_key_indicator'][$id]['entity']['paragraphs_item'][$item['value']]['field_key_indicator_text']);
        print '<div class="more">';
        print '<p>More about<br><span>';
        print $content['field_key_indicator'][$id]['entity']['paragraphs_item'][$item['value']]['field_key_indicator_title']['#items'][0]['safe_value'];
        print '</span></p>';
        print '<p class="plus">›</p>';
        print '</div>';
        print '</a>';
        print '</div>';
      }
      print '</div>';
      print '</div>';
    }

    if ($showtableau) {
      $item = $content['field_key_indicator']['#items'][$_GET['tableau']]['value'];
      print '<div class="tableau tableau-' . $item . ' active loaded">';
      _uw_theme_strategic_plan_key_tab_tableau($content, $item);
      print '</div>';

    }
  }
}

/**
 *
 */
function _uw_theme_strategic_plan_key_tab_tableau($content, $item) {
  print render($content['field_key_indicator'][$_GET['tableau']]['entity']['paragraphs_item'][$item]['field_linked_tableau']);
  print '<p class="back"><a href="?tab=key#tabs-nav">&lt; Back to progress dashboards</a></p>';
}

/**
 *
 */
function _uw_theme_strategic_plan_other_tab($content, $tab, $paragraph) {
  if (isset($_GET['tab']) && $_GET['tab'] == (string) $tab) {
    print '<h2 class="element-invisible">' . check_plain($content['field_tab'][$tab]['entity']['paragraphs_item'][$paragraph]['field_tab_title']['#items'][0]['value']) . '</h2>';
    print render($content['field_tab'][$tab]['entity']['paragraphs_item'][$paragraph]['field_tab_body']);
  }
}

/**
 *
 */
function _uw_theme_strategic_plan_all_tab($content) {
  if (isset($_GET['tab']) && $_GET['tab'] == 'all') {
    ?>
    <h2 class="element-invisible">All indicators</h2>
    <div id="tab-all-nav">
    <?php
    $showtableau = isset($_GET['tableau']) && !empty($content['field_indicator'][$_GET['tableau']]);
    $lastwasheader = FALSE;
    $hadheader = FALSE;
    $foundactive = FALSE;
    $loop = FALSE;
    print '<ul>';
    foreach ($content['field_indicator']['#items'] as $id => $item) {
      $paragraph = $content['field_indicator'][$id]['entity']['paragraphs_item'][$item['value']];
      if ($paragraph['#bundle'] == 'indicators_tableau' || $paragraph['#bundle'] == 'indicators_content') {
        if ($lastwasheader) {
          print '<ul>';
        }
        print '<li><a href="?tab=all&tableau=' . $id . '#tabs-nav"';
        if (!$foundactive) {
          if (!$showtableau || $_GET['tableau'] == $id) {
            print ' class="active"';
            $foundactive = TRUE;
          }
        }
        print '>';
        print check_plain($paragraph['field_link_title']['#items'][0]['value']);
        print '</a></li>';
        $lastwasheader = FALSE;
      }
      elseif ($paragraph['#bundle'] == 'indicators_header') {
        if (!$lastwasheader && $hadheader) {
          print '</ul>';
        }
        print '<li><div class="header">';
        print check_plain($paragraph['field_header']['#items'][0]['value']);
        print '</div>';
        $lastwasheader = TRUE;
        $hadheader = TRUE;
      }
      else {
        // Horrible fallback for things that didn't exist when this was written
        // (add code ASAP if this hits)
        if ($lastwasheader) {
          print '<ul>';
        }
        print '<li>';
        print render($paragraph);
        print '</li>';
        $lastwasheader = FALSE;
      }
      $loop = TRUE;
    }
    if ($hadheader) {
      print '</ul>';
    }
    print '</ul>';
    ?>
    </div>
    <div id="tab-all-display"<?php print (isset($_GET['tableau']) && !empty($content['field_indicator'][$_GET['tableau']])) ? ' class="active"' : ''; ?>>
      <?php
      if (isset($_GET['tableau']) && !empty($content['field_indicator'][$_GET['tableau']])) {
        print '<div class="tableau tableau-' . check_plain($_GET['tableau']) . ' active loaded">';
        _uw_theme_strategic_plan_all_tab_tableau($content, $_GET['tableau']);
        print '</div>';
      }
      ?>
    </div>
    <?php
  }
}

/**
 *
 */
function _uw_theme_strategic_plan_all_tab_tableau($content, $tableau) {
  print render($content['field_indicator'][$tableau]);
}
