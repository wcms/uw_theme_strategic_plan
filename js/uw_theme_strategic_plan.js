/**
 * @file
 * Adds JavaScript for "friendlier" behaviours when JS is available.
 */

(function ($) {
  "use strict";
  Drupal.behaviors.uwThemeStrategicPlan = {
    attach: function (context, settings) {
      // Toggle a class of "open" or "closed" on the "all indicators" subnav.
      // CSS will take care of the rest.
      // Default everything to closed.
      $('#tab-all-nav ul:not(.open)').addClass('closed');
      // Toggle open/closed when clicking the active item.
      $('#tab-all-nav ul a.active').live('click', function () {
        var $ul = $(this).closest('ul');
        if ($ul.hasClass('closed')) {
          $ul.addClass('open').removeClass('closed');
        }
else {
          $ul.addClass('closed').removeClass('open');
        }
      });
      // Close when a non-active item is selected.
      $('#tab-all-nav ul a:not(.active)').live('click', function () {
        $(this).closest('ul').addClass('closed').removeClass('open');
      });
    }
  };
}(jQuery));
