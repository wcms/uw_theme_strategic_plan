<?php

/**
 * @file
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>

    <?php print $user_picture; ?>

    <?php if ($display_submitted): ?>
      <div class="submitted"><?php print $date; ?> — <?php print $name; ?></div>
    <?php endif; ?>

    <div class="content_node"<?php print $content_attributes; ?>>
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      // Hide things from the content type that we're going to do something else with.
      hide($content['field_theme_area']);
      hide($content['field_indicator']);
      hide($content['field_tab']);
      hide($content['field_key_indicator']);
      print render($content);
      // Determine first clickable link in "all indicators" tab.
      $tableau = '';
      $key = 0;
      while (isset($content['field_indicator'][$key])) {
        foreach ($content['field_indicator'][$key]['entity']['paragraphs_item'] as $subkey => $subitem) {
          if (isset($subitem['field_link_title'])) {
            // Found the first link, set the variable.
            $tableau = $key;
            // Stop looking for a link.
            break 2;
          }
        }
        $key++;
      }
      ?>
      <ul id="tabs-nav">
        <li<?php if (!isset($_GET['tab']) || $_GET['tab'] == 'key') {print ' class="active"';
       } ?>><a href="?tab=key#tabs-nav">Progress dashboards</a></li>
        <?php
        if (!empty($content['field_tab']['#items'])) {
          foreach ($content['field_tab']['#items'] as $id => $item) {
            print '<li';
            if (isset($_GET['tab']) && $_GET['tab'] == (string) $id) {
              print ' class="active"';
            }
            print '>';
            print '<a href="?tab=' . $id . '#tabs-nav">';
            print check_plain($content['field_tab'][$id]['entity']['paragraphs_item'][$item['value']]['field_tab_title']['#items'][0]['value']);
            print '</a>';
            print '</li>';
            print "\r\n";
          }
        }
        ?>
        <li<?php if (isset($_GET['tab']) && $_GET['tab'] == 'all') {print ' class="active"';
       } ?>><a href="?tab=all&tableau=<?php echo $tableau; ?>#tabs-nav">All indicators</a></li>
      </ul>
      <div id="tabs">
        <div id="tab-key"<?php if (!isset($_GET['tab']) || $_GET['tab'] == 'key') {print ' class="active loaded"';
       } ?>>
          <?php
          _uw_theme_strategic_plan_key_tab($content);
          ?>
        </div>
        <?php
        if (!empty($content['field_tab']['#items'])) {
          foreach ($content['field_tab']['#items'] as $id => $item) {
            print '<div id="tab-' . $id . '" class="tabs-add';
            if (isset($_GET['tab']) && $_GET['tab'] == (string) $id) {
              print ' active loaded';
            }
            print '">';
            _uw_theme_strategic_plan_other_tab($content, $id, $item['value']);
            print '</div>';
          }
        }
        ?>
        <div id="tab-all"<?php if (isset($_GET['tab']) && $_GET['tab'] == 'all') {print ' class="active loaded"';
       } ?>>
          <?php
          _uw_theme_strategic_plan_all_tab($content);
          ?>
        </div>
      </div>
    </div>

    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>

    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>

  </div> <!-- /node-inner -->
</div> <!-- /node-->
<style>
#key-indicators > div:nth-child(2n+1) .field-name-field-key-indicator-text {
  background-image: url(<?php print image_style_url('theme_icon__landing_page_', $content['field_theme_area']['#items'][0]['taxonomy_term']->field_theme_page_icon[LANGUAGE_NONE][0]['uri']) ?>);
}
@media (min-width: 660px) and (max-width: 977px) {
  #key-indicators > div:nth-child(2n+1) .field-name-field-key-indicator-text {
    background-image: none;
  }
  #key-indicators > div:nth-child(4n+1) .field-name-field-key-indicator-text,
  #key-indicators > div:nth-child(4n+4) .field-name-field-key-indicator-text {
    background-image: url(<?php print image_style_url('theme_icon__landing_page_', $content['field_theme_area']['#items'][0]['taxonomy_term']->field_theme_page_icon[LANGUAGE_NONE][0]['uri']) ?>);
  }
}
</style>

<?php print render($content['comments']); ?>
