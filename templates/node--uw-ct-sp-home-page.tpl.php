<?php

/**
 * @file
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>

    <?php print $user_picture; ?>

    <?php if ($display_submitted): ?>
      <div class="submitted"><?php print $date; ?> — <?php print $name; ?></div>
    <?php endif; ?>

    <div class="content_node"<?php print $content_attributes; ?>>
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_centre_text']);
      print render($content);
      ?>
      <div id="circle-navigation">
        <?php
        print render($content['field_centre_text']);
        $themes = taxonomy_get_tree(taxonomy_vocabulary_machine_name_load('uw_tax_themes')->vid);
        $styles = '<style>' . "\r\n";
        print '<ul>';
        foreach ($themes as $count => $theme) {
          $term = taxonomy_term_load($theme->tid);
          print '<li>';
          $term->name = decode_entities(str_replace(' - ', '&nbsp;-&nbsp;', $term->name));
          // Keep "dashed" terms together.
          $link = l($term->name, 'node/' . $term->field_linked_page[LANGUAGE_NONE][0]['nid']);
          preg_match_all('/<a(.*)>(.*?)<\/a>/', $link, $finds);
            // Grab the content between the<a>*</a>.
          $link = str_replace($finds[2][0], '<span>' . $finds[2][0] . '</span>', $link);
           // Wrap it in a </span>.
          print $link;
          print '</li>';
          $styles .= '#circle-navigation li' . str_repeat(' + li', $count) . ' a { background-image: url(' . image_style_url('theme_icon__home_page_', $term->field_home_page_icon[LANGUAGE_NONE][0]['uri']) . '); }' . "\r\n";
          $styles .= '#circle-navigation li' . str_repeat(' + li', $count) . ' a:hover, #circle-navigation li' . str_repeat(' + li', $count) . ' a:focus { background-image: url(' . image_style_url('theme_icon__home_page_', $term->field_home_page_icon_hover_state[LANGUAGE_NONE][0]['uri']) . '); }' . "\r\n";
        }
        print '</ul>';
        $styles .= '</style>';
        // kpr($themes);
        print $styles;
        ?>
      </div>
    </div>

    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>

    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>

  </div> <!-- /node-inner -->
</div> <!-- /node-->

<?php print render($content['comments']); ?>
