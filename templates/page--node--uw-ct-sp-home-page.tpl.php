<?php

/**
 * @file
 */
?>
<div class="breakpoint"></div>
<div id="site" data-nav-visible="false" class="<?php print $classes; ?> uw-site"<?php print $attributes; ?>>
<div class="uw-site--inner">
    <div class="uw-section--inner">
      <div id="skip" class="skip">
          <a href="#main" class="element-invisible element-focusable uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
          <a href="#footer" class="element-invisible element-focusable  uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
      </div>
    </div>
    <div id="header" class="uw-header--global">
      <div class="uw-section--inner">
        <?php print render($page['global_header']); ?>
      </div>
    </div>
    <div id="site--offcanvas" class="uw-site--off-canvas <?php
      if (variable_get('uw_theme_branding', 'full') !== 'full') {
        print " generic_header";
      }
      else {
        print " non_generic_header";
      }
      ?>">
      <div class="uw-section--inner">
        <?php print render($page['site_header']); ?>
      </div>
    </div>
    <?php if (variable_get('uw_theme_branding', 'full') == "full") { ?>
    <div id="site-colors" class="uw-site--colors">
            <div class="uw-section--inner">
                <div class="uw-site--cbar">
                    <div class="uw-site--c1 uw-cbar"></div>
                    <div class="uw-site--c2 uw-cbar"></div>
                    <div class="uw-site--c3 uw-cbar"></div>
                    <div class="uw-site--c4 uw-cbar"></div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div id="site-header" class="uw-site--header">
      <div class="uw-section--inner">
        <a href="/strategic-plan" title="Strategic plan" rel="home">
          Strategic Plan
        </a>
      </div>
    </div>
    <div id="main" class="uw-site--main" role="main">
      <div class="uw-section--inner">
        <div class="uw-site--main-top">
          <div class="uw-site--messages">
              <?php print $messages; ?>
          </div>
          <div class="uw-site--help">
              <?php print render($page['help']); ?>
          </div>
          <div class="uw-site--breadcrumb">
              <?php print $breadcrumb; ?>
          </div>
          <div class="uw-site--title">
              <?php list($title, $content) = uw_fdsu_theme_separate_h1_and_content($page, $title); ?>
              <h1><?php print $site_name; ?></h1>
          </div>
        </div>
          <!-- when logged in -->
          <?php if ($tabs): ?>
            <div class="node-tabs uw-site-admin--tabs"><?php print render($tabs); ?></div><?php
          endif; ?>
            <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
        <div class="uw-site-main--content">
          <div id="content" class="uw-site-content">
            <div id="background-wrap">
              <?php print $content; ?>
            </div>
          </div><!--/main-content-->
        </div>
      </div><!--/section inner-->
    </div><!--/site main-->
    <div id="footer" class="uw-footer" role="contentinfo">
      <div id="uw-site-share" class="uw-site-share">
        <div class="uw-section--inner">
          <div class="uw-section-share">
          </div>
          <ul class="uw-site-share-top">
            <li class="uw-site-share--button__top">
              <a href="#" id="uw-top-button" class="uw-top-button">
              <i class="ifdsu fdsu-arrow"></i>
              <span class="uw-footer-top-word">TOP</span></a>
            </li>
            <li class="uw-site-share--button__share">
              <a href="#" class="uw-footer-social-button">
              <i class="ifdsu fdsu-share"></i>
              <span class="uw-footer-share-word">Share</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div id="site-footer" class="uw-site-footer site-footer-toggle open-site-footer">
          <div class="uw-section--inner">
            <?php if (empty($page['site_footer'])): ?>
            <div class="uw-site-footer1 uw-no-site-footer">
            <?php else : ?>
            <div class="uw-site-footer1">
            <?php endif; ?>
            <div class="uw-site-footer1--logo-dept">
                <?php
                    $site_logo = variable_get('uw_nav_site_footer_logo');
                    $site_logo_link = variable_get('uw_nav_site_footer_logo_link');
                    $facebook = variable_get('uw_nav_site_footer_facebook');
                    $twitter = variable_get('uw_nav_site_footer_twitter');
                    $instagram = variable_get('uw_nav_site_footer_instagram');
                    $youtube = variable_get('uw_nav_site_footer_youtube');
                    $linkedin = variable_get('uw_nav_site_footer_linkedin');
                ?>

                <?php if (isset($site_logo_link)) : ?>
                    <a href="<?php print $site_logo_link; ?>"><img src="<?php print base_path() . drupal_get_path('module', 'uw_nav_site_footer'); ?>/logos/<?php print variable_get('uw_nav_site_footer_logo'); ?>.png" /></a>
                <?php else : ?>
                    <?php  $site_name = (strtolower($site_name)); print '<a href="' . url('<front>') . '">' . (ucfirst($site_name)) . '</a>';  ?>
                <?php endif; ?>

            </div>
            <div class="uw-site-footer1--contact">
                <ul class="uw-footer-social">
                    <?php if($facebook !== "" && $facebook !== NULL): ?>
                        <li>
                            <a href="https://www.facebook.com/<?php print check_plain($facebook); ?>">
                                <i class="ifdsu fdsu-facebook"></i>
                                <span class="off-screen">facebook</span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($twitter !== "" && $twitter !== NULL): ?>
                        <li>
                            <a href="https://www.twitter.com/<?php print check_plain($twitter); ?>">
                                <i class="ifdsu fdsu-twitter"></i>
                                <span class="off-screen">twitter</span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($youtube !== "" && $youtube !== NULL): ?>
                        <li>
                            <a href="<?php print check_plain($youtube); ?>">
                                <i class="ifdsu fdsu-youtube"></i>
                                <span class="off-screen">youtube</span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($instagram !== "" && $instagram !== NULL): ?>
                        <li>
                            <a href="<?php print check_plain($instagram); ?>">
                                <i class="ifdsu fdsu-instagram"></i>
                                <span class="off-screen">instagram</span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($linkedin !== "" && $linkedin !== NULL): ?>
                        <li>
                            <a href="<?php check_plain(print $linkedin); ?>">
                                <i class="ifdsu fdsu-linkedin"></i>
                                <span class="off-screen">linkedin</span>
                            </a>
                        </li>
                    <?php endif; ?>

                </ul>

            </div>
            </div>
            <?php if (!empty($page['site_footer'])): ?>
              <div class="uw-site-footer2">
                <?php print render($page['site_footer']); ?>
              </div>
            <?php endif; ?>
          </div>
      </div>
       <?php print render($page['global_footer']); ?>
     </div>
  </div>
</div><!--/site-->
<div class="ie-resize-fix"></div>
